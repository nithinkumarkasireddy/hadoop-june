package com.nithin;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

public class simpleUDF extends UDF {
	public Text evaluate(Text input){
		if(input == null)
			return null;
		return new Text("Hello" + input.toString());
		
	}

}
